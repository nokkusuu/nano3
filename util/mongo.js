const mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , db = {};

const config = require('../cfg.json');

mongoose.connect(process.env.MONGO, {useNewUrlParser: true});

const userSchema = new Schema({
    '_id': String,
    'data': {
        'cooldowns': {
            'daily': { type: Number, default: 0 },
            'vote': { type: Number, default: 0 }
        },
        'yen': { type: Number, default: 0 },
        'memo': { type: String, default: 'Set a memo with nano memo.'},
        'commandsRun': { type: Number, default: 0 },
        'rank': String
    },
    'collectibles': [{
        'id': String,
        'name': String,
        'desc': String,
        'image': String,
        'transferable': Boolean
    }],
    'backgrounds': [{
        'id': String,
        'timestamp': Date,
        'tradeable': Boolean
    }]
});

class Database {
    constructor() {
        this.user = mongoose.model('user', userSchema);
    }

    async setUserMemo(id, memo) {
        return new Promise((resolve, reject) => {
            this.user.find({ _id: id }, (err, docs) => {
                if (docs.length < 1) {
                    let newUser = new this.user();
                    newUser._id = id;
                    newUser.data.memo = memo;
                    newUser.save();
                    resolve();
                } else {
                    docs[0].data.memo = memo;
                    docs[0].save();
                    resolve();
                }
                if (docs.length > 1) {
                    console.error('ERROR: there are more than one user object for a specific id!');
                }
            })
        })
    }

    async doUserDaily(id) {
        return new Promise((resolve, reject) => {
            this.user.find({ _id: id }, (err, docs) => {
                if (docs.length < 1) {
                    let newUser = new this.user();
                    newUser._id = id;
                    newUser.data.cooldowns.daily = Date.now();
                    newUser.data.yen = config.bot.yenRate;
                    newUser.save();
                    resolve({
                        'status': 'success',
                        'yen': config.bot.yenRate
                    });
                } else {
                    if ((Date.now() - docs[0].data.cooldowns.daily) > 86400000) {
                        docs[0].data.cooldowns.daily = Date.now();
                        docs[0].data.yen += config.bot.yenRate;
                        docs[0].save();
                        resolve({
                            'status': 'success',
                            'yen': docs[0].data.yen
                        });
                    } else {
                        resolve({
                            'status': 'failureNotReady',
                            'yen': docs[0].data.yen
                        });
                    }
                }
            })
        })
    }

    // TODO: actually make this do things!
    async doUserLoot(id, price) {
        return new Promise((resolve, reject) => {
            this.user.find({ _id: id }, (err, docs) => {
                if (docs.length < 1) {
                    resolve({
                        'status': 'failureInsufficient',
                        'acquire': null
                    });
                } else {
                    if (docs[0].data.yen >= price) {
                        let selected = {
                            'identifier': 'COM_yuuko',
                            'name': 'Yuuko',
                            'rarity': 'Common'
                        };
                        resolve({
                            'status': 'success',
                            'acquire': selected
                        })
                    } else {
                        resolve({
                            'status': 'failureInsufficient',
                            'acquire': null
                        });
                    }
                }
            });
        })
    }
}

module.exports = new Database;