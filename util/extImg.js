// based loosely on an old function i made like six months ago
module.exports = async (message) => {
    return new Promise(async (resolve, reject) => {
        let url = message.cleanContent.match(/(https?:\/\/.*\.(?:png|jpg|jpeg))/i);
        if (url) {
            resolve(url[0]);
        } else if (message.attachments.first()) {
            if (!message.attachments.first().url.match(/(https?:\/\/.*\.(?:png|jpg|jpeg))/i)) {
                resolve();
            }
            if (message.attachments.first().width > 0 && message.attachments.size > 0) {
                resolve(message.attachments.first().url);
            }
        } else {
            resolve();
        }
    })
}