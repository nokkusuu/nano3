const fs = require('fs')
    , config = require('../cfg.json');

function walkDir(dir, ft) {
    if (!dir.endsWith('/')) {dir += '/'};
    let files = [];
    fs.readdirSync(dir).forEach((fil) => {
        let stat = fs.lstatSync(dir + fil);
        if (stat.isDirectory()) {
            files = files.concat(walkDir(dir + fil + '/'));
        } else if (stat.isFile && fil.endsWith('.js')) {
            files.push(dir + fil);
        }
    });
    return files;
}

const cmdFiles = walkDir(config.bot.cmdFolder);
var cmdArr = [];
var nameArr = [];
cmdFiles.forEach((fil) => {
    let cmd = require('.' + fil);
    cmdArr.push(cmd);
    nameArr.push(cmd.info.alias);
})

module.exports = {
    'cmds': cmdArr,
    'names': nameArr
}