/*
* nano 3.0
* created by nox#2530
*/

// load dotenv for secrety secrets
require('dotenv').config();

// load discord.js module and create a clients
const Discord = require('discord.js')
    , client = new Discord.Client();

// get mongoose started
const db = require('./util/mongo.js');

// make things pretty
const chalk = require('chalk');

// load config file. do not put tokens in this, as it is publicly visible!
const config = require('./cfg.json');

// command handler
const commands = require('./util/cmdLoader.js');
console.log(chalk.green('loaded commands'));

// didyoumean
const didyoumean = require('didyoumean2').default;

// when the client receives a message...
client.on('message', async (message) => {
    // if the message's author is a bot or doesn't start with the prefix, return
    // anything above this line will be executed regardless of these rules
    if (message.author.bot || !message.content.startsWith(config.bot.prefix) || message.system) return;
    // may support dm channels in the future, but for now...
    if (message.channel.type != 'text') return;

    // parse message content
    let args = [];
    message.content.slice(config.bot.prefix.length).split(/( |\n)+/g).forEach(el => {
        if (el == ' ' || el == '\n') {
            return;
        } else {
            args.push(el);
        }
    });

    let cmd = args.shift().toLowerCase();

    // find commands in array of commands
    let command = commands.cmds.find(x => x.info.alias === cmd);

    // didyoumean
    if (!command) {
        let match = didyoumean(cmd, commands.names, {
            'threshold': '0.6'
        });
        if (match) {
            message.channel.send(`<@${message.author.id}>, did you mean **${config.bot.prefix}${match}?**`);
        }
        return;
    };

    // if a command requires developer permissions, check the user id against the dev list
    if (command.info.reqPerms.includes('DEVELOPER')) {
        if (!config.bot.developers.includes(message.author.id)) {
            return message.channel.send(`<@${message.author.id}>, that command is for developer use only.`);
        }
    }

    // discord.js hates the DEVELOPER permission and errors out, so we have to remove it after it's used
    let djsPerms = command.info.reqPerms.pop(command.info.reqPerms.indexOf('DEVELOPER'));

    // if a command requires other permissions, check the user's permissions against them
    if (!message.member.hasPermission(command.info.reqPerms)) {
        return message.channel.send(`<@${message.author.id}>, you do not have permission to run that command.`);
    }

    // testing
    for (_i = 0; _i < command.info.reqPerms.length; _i++) {
        if (!message.member.hasPermission(command.info.reqPerms[_i])) {
            return message.channel.send(`<@${message.author.id}>, you do not have permission to run that command.`);
        }
    }

    // log the command to the console for debugging purposes
    logMessage(message); 

    // data to pass on to the command
    let data = {
        "client": client,
        "message": message,
        "commands": commands,
        "args": args,
        "longArg": args.slice(cmd).join(' ').trim(),
        "cleanArg": message.cleanContent.slice(config.bot.prefix.length + cmd.length).trim(),
        "config": config,
        "db": db
    }

    // execute command
    try {
        command.exec(data);
    } catch(err) {
        message.channel.send('An error has occurred.'); // better than not saying anything at all
        console.error(err);
    }
});

// when the client logs in...
client.on('ready', async () => {
    // changes status every x milliseconds, x being bot.statusRate in cfg.json
    client.setInterval(() => {
        // change this variable to whatever you want. also note it uses a special type of string :O
        let status = `${config.bot.prefix}help // in ${client.guilds.size} servers`;
        client.user.setPresence({ game: { name: status }, status: 'online' });
        
    }, config.bot.statusRate); 

    console.log(chalk.green('ready'));

    // let bot owner know that the bot has restarted
    client.fetchUser(config.bot.owner).then(r => {
        try {
            ownerUser.send('Bot has started!');
        }catch(err){
            // nobody cares, just do nothing
        }
    });
});

// function for logging messages/commands
function logMessage(message) {
    if (message.author.bot) {
        console.log(`${chalk.blue(message.author.tag + ' [bot]')}: ${chalk.yellow(message.cleanContent)}`);
    } else {
        console.log(`${chalk.blue(message.author.tag)}: ${chalk.yellow(message.cleanContent)}`);
    }
}
    
// log in with token (found in .env file)
client.login(process.env.TOKEN);