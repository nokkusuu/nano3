exports.info = {
    "alias": "weeb",
    "desc": "Gets an image/gif from the weeb.sh API.",
    "usage": "weeb <category> [nsfw]",
    "category": "Images & GIFs",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , args = data.args
        , longarg = data.longArg
        , config = data.config;

    let weeb = require('../../data/weeb.json')
        , needle = require('needle')
        , querystring = require('querystring');
    
    let options = {};

    if (longarg.length < 1) {
        return message.channel.send(`<@${message.author.id}>, you must specify a type. To get a list of types, use \`${config.bot.prefix}${exports.info.alias} types\`.`)
    } 
    if (args[0] == 'types') {
        return message.channel.send(`Supported types:\n${'`'+weeb.types.join('`, `')+'`'}`);
    } 
    if (!weeb.types.includes(args[0])) {
        return message.channel.send(`<@${message.author.id}>, the specified type could not be found.`);
    } 

    options.type = args[0]; args.shift();

    if (args.includes('nsfw') && !message.channel.nsfw) {
    }

    if (args.includes('nsfw')) {
        if (message.channel.nsfw) {
            options.nsfw = 'true';
        } else {
            return message.channel.send(`<@${message.author.id}>, you cannot request NSFW content in a non-nsfw channel.`);
        }
    }

    needle('get', `https://api.weeb.sh/images/random?${querystring.stringify(options)}`, {
        'headers': {
            'Authorization': process.env.WOLKE,
            'User-Agent': config.bot.useragent
        }
    }).then(r => {
        message.channel.send({
            'embed': {
                'image': {
                    'url': r.body.url
                },
                'footer': {
                    'icon_url': 'https://nanodiscord.me/cdn/weebsh-logo.png',
                    'text': 'Powered by Weeb.sh'
                }
            }
        })
    })
}