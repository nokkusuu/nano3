exports.info = {
    "alias": "purge",
    "desc": "Removes a specified amount of messages.\nWARNING: Also removes command message!",
    "usage": "purge <amount>",
    "category": "Moderation",
    "hideInHelp": false,
    "reqPerms": ['MANAGE_MESSAGES']
}

exports.exec = (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , args = data.args
        , config = data.config;

    // make sure amount specified is valid
    if (!parseInt(args[0])) return message.channel.send(`<@${message.author.id}>, you must specify an amount of messages to purge.`);
    let amount = parseInt(args[0]);
    if (amount > config.bot.maxPurgeAmount) return message.channel.send(`<@${message.author.id}>, the largest amount of messages you can purge at once is ${config.bot.maxPurgeAmount}.`);

    // it's purge time
    message.channel.fetchMessages({ limit: (amount+1) })
    .then(messages => {
        message.channel.bulkDelete(messages);
    });
    message.channel.send(`<@${message.author.id}>, successfully deleted ${amount} messages.`)
    .then(message => {
        message.delete(3000);
    });
}