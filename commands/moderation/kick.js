exports.info = {
    "alias": "kick",
    "desc": "Kicks a user from the server.",
    "usage": "kick <mention>",
    "category": "Moderation",
    "hideInHelp": false,
    "reqPerms": ['KICK_MEMBERS']
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message;

    let user = message.mentions.members.first();
    if (!message.mentions.members.first()) {
        message.channel.send('You must specify a user to kick.')
    } else {
        try {
            let tag = message.mentions.users.first().tag;
            await message.mentions.members.first().kick(`Banned by ${message.author.tag}`);
        } catch(err) {
            if (err) {
                message.channel.send(`Unable to kick user from guild.`);
                return;
            }
        }
        message.channel.send(`Successfully kicked user from guild.`);
    }
}