exports.info = {
    "alias": "ban",
    "desc": "Bans a user from the server.",
    "usage": "ban <mentions/ids>",
    "category": "Moderation",
    "hideInHelp": false,
    "reqPerms": ['BAN_MEMBERS']
}

/*
exports.exec = async (data) => {
    const client = data.client
        , message = data.message;

    const user = message.mentions.members.first();
    if (!message.mentions.members.first()) {
        message.channel.send('You must specify a user to ban.')
    } else {
        try {
            let tag = message.mentions.users.first().tag;
            await message.mentions.members.first().ban(`Banned by ${message.author.tag}`);
        } catch(err) {
            if (err) {
                message.channel.send(`Unable to ban user from guild.`);
                return;
            }
        }
        message.channel.send(`Successfully banned user from guild.`);
    }
}
*/

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , args = data.args;

    var users = [];
    var success = 0;
    var fail = 0;
    args.forEach(async (el) => {
        if (RegExp(/<@!?[0-9]+>/g).test(el)) {
            // argument is mention
            client.fetchUser(el.match(/[0-9]+/g)[0]).then(user => {
                if (!user) {
                    fail+=1;
                } else {
                    users.push(user);
                    success+=1;
                }
            })
        } else if (RegExp(/[0-9]+/g).test(el)) {
            // argument is id
            client.fetchUser(el).then(user => {
                if (!user) {
                    fail+=1;
                } else {
                    users.push(user);
                    success+=1;
                }
            });
        } else {
            // argument is neither
            console.log(el);
            fail+=1;
        }
    });

    message.channel.send({
        embed: {
            'title': 'Ban Report',
            'description': `${success} bans succeeded\n${fail} bans failed`
        }
    })

    console.log(users);

}