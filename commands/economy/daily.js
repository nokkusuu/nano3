exports.info = {
    "alias": "daily",
    "desc": "Grants the user a daily amount of currency.",
    "usage": "daily",
    "category": "Economy & Profile",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , db = data.db;

    let daily = await db.doUserDaily(message.author.id)
    switch(daily.status) {
        case 'success':
            message.channel.send(`<@${message.author.id}>, you now have ¥${daily.yen}!`);
            break;
        case 'failureNotReady':
            message.channel.send(`<@${message.author.id}>, you must wait TIME.`);
    }
}