exports.info = {
    "alias": "jpegify",
    "desc": "Uses JPEG compression to ruin images.",
    "usage": "jpegify <image>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;

    let jimp = require('jimp')
        , needle = require('needle')
        , extimg = require('../../util/extImg.js');
    
    let url = await extimg(message);
    if (!url) {
        return message.channel.send('You must specify an image to jpegify. (GIFs are not supported)');
    }
    let image = await needle('get', url);

    jimp.read(image.body).then(async (img) => {
        let msg = await message.channel.send('Processing...')
        img.quality(4);
        
        msg.delete();
        message.channel.send({ 
            files: [{
                attachment: await img.getBufferAsync('image/jpeg'),
                name: 'jpegify.jpg'
            }]
        });
    })
}