//823x478
exports.info = {
    "alias": "desktop",
    "desc": "Stretch.",
    "usage": "desktop <image>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;

    let jimp = require('jimp')
        , needle = require('needle')
        , imagesize = require('image-size')
        , extimg = require('../../util/extImg.js');
    
    let url = await extimg(message);
    if (!url) {
        return message.channel.send(`<@${message.author.id}>, you must specify an image to generate a shirt image. (GIFs are not supported)`);
    }

    let msg = await message.channel.send('Processing...')

    let image = await jimp.read((await needle('get', url)).body);
    let wix = await jimp.read('./assets/desktop.png');

    image.resize(823,478);

    wix.composite(image, 0, 0, {
        mode: jimp.BLEND_DESTINATION_OVER
    });

    msg.delete();
    message.channel.send({ 
        files: [{
            attachment: await wix.getBufferAsync('image/jpeg'),
            name: 'wix.jpg'
        }]
    });

}