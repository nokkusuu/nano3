exports.info = {
    "alias": "antimagik",
    "desc": "The opposite of magik.",
    "usage": "antimagik <image>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;

    let jimp = require('jimp');

    let gm = require('gm').subClass({imageMagick: true})
        , needle = require('needle')
        , extimg = require('../../util/extImg.js');
    
    let url = await extimg(message);
    if (!url) {
        return message.channel.send('You must specify an image to use magik. (GIFs are not supported)');
    }
    let image = await needle('get', url);

    let img = await jimp.read(image.body);
    let height = (img.bitmap.height / img.bitmap.width) * 150;
    img.resize(150,height);

    let msg = await message.channel.send('Processing...')
    gm(await img.getBufferAsync('image/jpeg')).in('-liquid-rescale', '180x180%').toBuffer('PNG', (err, buf) => {
        if (err) {
            message.channel.send('An error has occurred while processing your image.');
            throw err;
        }
        gm(buf).in('-liquid-rescale', '180x180%').toBuffer('PNG', async (err, qbuf) => {
            if (err) {
                message.channel.send('An error has occurred while processing your image.');
                throw err;
            }
            msg.delete();
            message.channel.send({
                files: [{
                    attachment: qbuf,
                    name: 'antimagik.png'
                }]
            });
        });
    });
}