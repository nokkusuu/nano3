//TODO: make magik resize before processing to make processing time shorter

exports.info = {
    "alias": "magik",
    "desc": "Similar to NotSoBot's famous command.",
    "usage": "magik <image>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;

    let gm = require('gm').subClass({imageMagick: true})
        , needle = require('needle')
        , extimg = require('../../util/extImg.js');
    
    let url = await extimg(message);
    if (!url) {
        return message.channel.send('You must specify an image to use magik. (GIFs are not supported)');
    }
    let image = await needle('get', url);
    if (!image.body) return message.channel.send('Could not load image.');

    let msg = await message.channel.send('Processing...')
    gm(image.body).in('-liquid-rescale', '50x50%').toBuffer('PNG', (err, buf) => {
        if (err) {
            message.channel.send('An error has occurred while processing your image.').then(() => {
                throw err;
            });
        }
        msg.delete();
        message.channel.send({
            files: [{
                attachment: buf,
                name: 'magik.png'
            }]
        });
    });
}