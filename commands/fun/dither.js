exports.info = {
    "alias": "dither",
    "desc": "16-bit color time.",
    "usage": "dither <image>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;

    let jimp = require('jimp')
        , needle = require('needle')
        , extimg = require('../../util/extImg.js');
    
    let url = await extimg(message);
    if (!url) {
        return message.channel.send(`<@${message.author.id}>, you must specify an image to use edgedetect. (GIFs are not supported)`);
    }
    let image = await needle('get', url);
    
    jimp.read(image.body).then(async (img) => {
        let msg = await message.channel.send('Processing...')
        img.dither565;
        msg.delete();
        message.channel.send({ 
            files: [{
                attachment: await img.getBufferAsync('image/jpeg'),
                name: 'dither.jpg'
            }]
        });
    })
}