// note: i'm not a furry; i just wanted to test out replacing with a callback
// the amount of regex in this probably makes it a super expensive process
exports.info = {
    "alias": "owo",
    "desc": "Hewwo...?",
    "usage": "owo <text>",
    "category": "Fun",
    "hideInHelp": true,
    "reqPerms": []
}

exports.exec = (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , cleanarg = data.cleanArg;

        function owoify (string) {
            function getCaseOfChar(char) {
                char=char[0];
                if (char == char.toLowerCase()) return 0;
                else if (char == char.toUpperCase()) 
                return 1; else return -1;
            }    
        
            let star = [];
            string.split('\n').forEach(el => {
                el=el.replace(/[lr]/gi, (mat, off, str) => {
                    switch (getCaseOfChar(mat)) {
                        case 0: return 'w'; break;
                        case 1: return 'W'; break;
                        default: return mat; break;
                    }
                })
                el=el.replace(/n(?=[aeou])/gi, (mat, off, str) => { // replace 'n' with 'ny' as long as it is followed by a vowel
                    if (getCaseOfChar(str[off+1]) == 1) return mat+'Y';
                    else return mat+'y';
                });    
                
                star.push(el);
            });
            return star.join('\n');
        }        
    
    message.channel.send(owoify(cleanarg));
}

