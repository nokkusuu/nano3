exports.info = {
    "alias": "shirt",
    "desc": "Turns any image into a shirt preview.",
    "usage": "shirt <image>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;

    let jimp = require('jimp')
        , needle = require('needle')
        , imagesize = require('image-size')
        , extimg = require('../../util/extImg.js');
    
    let url = await extimg(message);
    if (!url) {
        return message.channel.send(`<@${message.author.id}>, you must specify an image to generate a shirt image. (GIFs are not supported)`);
    }
    let image = await jimp.read((await needle('get', url)).body);

    jimp.read('./assets/shirt.png').then(async (img) => {
        let msg = await message.channel.send('Processing...')

        let height = (image.bitmap.height / image.bitmap.width) * 210;
        image.resize(210,height);

        img.composite(image, 100, 100, {
            mode: jimp.BLEND_MULTIPLY,
            opacitySource: 0.7,
            opacityDest: 1
        });
    
        msg.delete();
        message.channel.send({ 
            files: [{
                attachment: await img.getBufferAsync('image/jpeg'),
                name: 'shirt.jpg'
            }]
        });
    })
}