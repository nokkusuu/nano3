exports.info = {
    "alias": "thatshot",
    "desc": "It's rewind time.",
    "usage": "thatshot <image>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;

    let jimp = require('jimp')
        , needle = require('needle')
        , imagesize = require('image-size')
        , extimg = require('../../util/extImg.js');
    
    let url = await extimg(message);
    if (!url) {
        return message.channel.send(`<@${message.author.id}>, you must specify an image to generate a shirt image. (GIFs are not supported)`);
    }

    let msg = await message.channel.send('Processing...')

    let image = await jimp.read((await needle('get', url)).body);
    let hot = await jimp.read('./assets/thatshot.png');

    image.resize(628,347);

    hot.composite(image, 11, 384, {
        mode: jimp.BLEND_DESTINATION_OVER
    });

    msg.delete();
    message.channel.send({ 
        files: [{
            attachment: await hot.getBufferAsync('image/jpeg'),
            name: 'hot.jpg'
        }]
    });

}