exports.info = {
    "alias": "ifunny",
    "desc": "Adds the iFunny watermark to an image.",
    "usage": "ifunny <image>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;

    let jimp = require('jimp')
        , needle = require('needle')
        , imagesize = require('image-size')
        , extimg = require('../../util/extImg.js');
    
    let url = await extimg(message);
    if (!url) {
        return message.channel.send(`<@${message.author.id}>, you must specify an image to use ifunny. (GIFs are not supported)`);
    }

    let image = jimp.read((await needle('get', url)).body);
    let funnyR = jimp.read('./assets/ifunny-r.png');
    let funnyL = jimp.read('./assets/ifunny-l.png');
    await image; await funnyR; await funnyL;

    if (image.bitmap.width<600) {
        image.resize(600, (image.bitmap.height / image.bitmap.width) * 600);
    } else if (image.bitmap.width>1200) {
        image.resize(1200, (image.bitmap.height / image.bitmap.width) * 1200);
    }

    let msg = await message.channel.send('Processing...');

    funnyL.resize(image.bitmap.width, 20);
    image.composite(funnyL, 0, image.bitmap.height-20);
    image.composite(funnyR, image.bitmap.width-141, image.bitmap.height-20);

    try {
        msg.delete();
    } catch(err) {
        console.log('I don\'t have permission to delete messages');
    }
    message.channel.send({ 
        files: [{
            attachment: await image.getBufferAsync('image/png'),
            name: 'ifunny.jpg'
        }]
    });
    

}