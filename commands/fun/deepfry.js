exports.info = {
    "alias": "deepfry",
    "desc": "Absolutely destroys your images.",
    "usage": "deepfry <image>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;

    let jimp = require('jimp')
        , needle = require('needle')
        , extimg = require('../../util/extImg.js');
    
    let url = await extimg(message);
    if (!url) {
        return message.channel.send(`<@${message.author.id}>, you must specify an image to deepfry. (GIFs are not supported)`);
    }
    let image = await needle('get', url);

    jimp.read(image.body).then(async (img) => {
        let msg = await message.channel.send('Processing...')
        img.contrast(0.95).convolute([[0, -2, 0], [-2, 9, -1], [0, -2, 0]]).quality(1);
        msg.delete();
        message.channel.send({ 
            files: [{
                attachment: await img.getBufferAsync('image/jpeg'),
                name: 'deepfry.png'
            }]
        });
    })
}