exports.info = {
    "alias": "conch",
    "desc": "Ask the magic conch!\nSimilar to 8ball, but with a 10% chance of a positive answer.",
    "usage": "conch <question>",
    "category": "Fun",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , longArg = data.longArg;

    let emoji = '🐚';
    let responses = [
        'Maybe someday.',
        'I don\'t think so.',
        'Yes.',
        'Try asking again.',
        'No.',
        'Absolutely not.',
        'Not at all.',
        'Never.',
        'Nah.',
        'Negative.'
    ];
    
    if (!longArg) return message.channel.send(`<@${message.author.id}>, you must ask a question.`);
    message.channel.send(`${emoji} ${responses[Math.floor(Math.random() * responses.length)]}`);
}