exports.info = {
    "alias": "anime",
    "desc": "Gives information about any specified anime.",
    "usage": "anime <title>",
    "category": "Miscellaneous",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , longarg = data.longArg;
    
    // super low-profile function for converting integers to ordinal values
    function ordinal(i) {
        let j=i%10,k=i%100;
        if(j==1&&k!=11)return i+'st';
        if(j==2&&k!=12)return i+'nd';
        if(j==3&&k!=13)return i+'rd';
        return i+'th';
    }

    let needle = require('needle');

    // function to get kitsu stuff, because it might need to be reused at some point
    async function kitsu(title) {
        return new Promise(async (resolve, reject) => {
            // get basic info
            let main = await needle('get', `https://kitsu.io/api/edge/anime?filter[text]=${longarg}`);
            main = JSON.parse(main.body.toString());

            // use the first result and error if it doesn't exist
            let anime = main.data[0];
            if (!anime) resolve('err-nf');

            // fetch genres
            let genres = JSON.parse((await needle('get', anime.relationships.genres.links.related)).body.toString());

            // create object with data
            let obj = {};
            obj.id = anime.id;
            obj.title = anime.attributes.titles.en_jp;

            // description
            if (!anime.attributes.synopsis) {obj.description = 'No description specified.'}
            else {obj.description = anime.attributes.synopsis.substring(0, anime.attributes.synopsis.indexOf(".")) + "."};

            // rating
            if (!anime.attributes.averageRating) {obj.description = 'No rating'}
            else {obj.rating = (parseInt(anime.attributes.averageRating) / 10).toString()};

            // rank
            if (!anime.attributes.ratingRank) {obj.rank = 'Unranked'}
            else {obj.rank = anime.attributes.ratingRank.toString()};

            // image
            if (anime.attributes.posterImage.tiny) {obj.image = anime.attributes.posterImage.tiny};

            // status 
            if (!anime.attributes.status) {obj.status = 'Unknown'}
            else {obj.status = anime.attributes.status};

            // episodes
            if (!anime.attributes.episodeCount) {obj.episodes = '???'}
            else {obj.episodes = anime.attributes.episodeCount};

            // length
            if (!anime.attributes.episodeLength) {obj.length = '???'}
            else {obj.length = anime.attributes.episodeLength};

            // url
            if (!anime.links.self) {obj.url = ''}
            else {obj.url = anime.links.self};

            // age 
            if (!anime.attributes.ageRating && !anime.attributes.ageRatingGuide) {obj.age = 'Not rated'}
            else if (!anime.attributes.ageRatingGuide) {obj.age = anime.attributes.ageRating}
            else {obj.age = `${anime.attributes.ageRating} (${anime.attributes.ageRatingGuide})`};

            // genres
            if (!genres) {obj.genres = 'Genreless'}
            else {
                let genreList = [];
                for (i = 0; i < genres.meta.count; i++) {
                    genreList.push(genres.data[i].attributes.name);
                }
                obj.genres = genreList.join(', ');
            }

            let embed = {
                'title': obj.title,
                'description': obj.description,
                'url': obj.url,
                'thumbnail': {
                    'url': obj.image
                },
                'fields': [
                    {
                        'name': 'Rank',
                        'value': `${obj.rating}/10 (${ordinal(obj.rank)})`,
                        'inline': true
                    },
                    {
                        'name': 'Status',
                        'value': `${obj.episodes} episodes, each ${obj.length} minutes long **(${obj.status.charAt(0).toUpperCase() + obj.status.substr(1)})**`,
                        'inline': true
                    },
                    {
                        'name': 'Age Rating',
                        'value': obj.age,
                        'inline': true
                    },
                    {
                        'name': 'Genre',
                        'value': obj.genres,
                        'inline': true
                    }
                ]
            };

            resolve(embed);
        })
    }

    // if nothing is specified, nothing can be done
    if (!longarg) return message.channel.send(`<@${message.author.id}>, you must specify an anime.`);

    // get data and send it to discord
    let msg = await message.channel.send('Searching...');
    let dat = await kitsu(longarg);
    msg.delete();
    if (dat === 'err-nf') return message.channel.send(`<@${message.author.id}>, the specified anime could not be found.`);
    
    message.channel.send({
        embed: dat
    });
}