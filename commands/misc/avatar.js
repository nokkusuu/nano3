exports.info = {
    "alias": "avatar",
    "desc": "Get a user's avatar by mention or ID.",
    "usage": "avatar <mention/id>",
    "category": "Miscellaneous",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , longArg = data.longArg;

    let user = await client.fetchUser(longArg.match(/((?<=<@)\d+(?=>))|(\d+)/g));
    if (!user) return message.channel.send(`<@${message.author.id}>, you must specify a user.`);
    message.channel.send({
        'files': [{
            'attachment': user.avatarURL,
            'name': 'avatar.png'
        }]
    });
}