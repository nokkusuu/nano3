exports.info = {
    "alias": "help",
    "desc": "Shows list of commands, or gives info about any command.",
    "usage": "help <command>",
    "category": "Miscellaneous",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , longArg = data.longArg
        , config = data.config;

    if (longArg) {
        let cmd = commands.cmds.find((el) => {
            return (el.info.alias == longArg);
        })
        if (!cmd) {
            message.channel.send('The requested command could not be found.');
        } else {
            message.channel.send({
                'embed': {
                    'title': `Info for ${cmd.info.alias}`,
                    'description': cmd.info.desc,
                    'fields': [
                        {
                            'name': 'Usage',
                            'value': `\`${config.bot.prefix}${cmd.info.usage}\``
                        }
                    ]
                }
            })
        }
        return;
    }
              
    // building an embed using an object, heck yeah
    let embed = {
        "author": {
            "name": "Command List",
            "icon_url": client.user.avatarURL
        },
        'description': 'To get more information about a command, run `help <command>`.',
        'fields': [
            // i don't know why i left this space here but uhh i guess it's cool
        ]
    };
            
    commands.cmds.forEach((el) => {

        if (el.info.hideInHelp) return;

        let field = embed.fields.find((ql) => {
            return (ql.name == el.info.category);
        });

        if (!field) {
            embed.fields.push({
                'name': el.info.category,
                'value': `\`${el.info.alias}\` `
            })
        } else {
            embed.fields[embed.fields.indexOf(field)].value += `\`${el.info.alias}\` `;
        }
    });

    // embed.footer = { 'text': '💡 ' + config.bot.helpTips[Math.floor(Math.random() * config.bot.helpTips.length)] }

    message.channel.send({embed: embed});
}