exports.info = {
    "alias": "ping",
    "desc": "Returns the latency/delay time (ping).",
    "usage": "ping",
    "category": "Miscellaneous",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands;
    
    message.channel.send(`Pong! \`${client.ping}ms\``);
}