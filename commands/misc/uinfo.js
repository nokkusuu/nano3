exports.info = {
    "alias": "uinfo",
    "desc": "Get information about a user.",
    "usage": "uinfo <mention/id>",
    "category": "Miscellaneous",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , longArg = data.longArg
        , config = data.config;
    
    // dumbest way of finding out if a user exists, *ever*. thanks, discord.js for having terrible user fetch support.
    if (!RegExp(/((?<=<@)\d+(?=>))|((?<=^|\s)\d+(?=$| ))/g).test(longArg)) return message.channel.send(`<@${message.author.id}>, you must specify a user.`);
    try {
        await client.fetchUser(longArg.match(/((?<=<@)\d+(?=>))|((?<=^|\s)\d+(?=$| ))/g)[0]);
    } catch(err) {
        return message.channel.send(`<@${message.author.id}>, that user could not be found.`);
    }
    let user = await client.fetchUser(longArg.match(/((?<=<@)\d+(?=>))|(\d+)/g)[0]);

    // init info object
    let info = {};

    // get member
    let member = message.guild.members.get(user.id);
    info.member = member;
    if (!member) {
        info.inGuild = false;
    } else {
        info.inGuild = true;
    }

    // generate status
    switch (user.presence.status) {
        case 'online':
            info.status = 'Online';
            break;
        case 'offline':
            info.status = 'Offline';
            break;
        case 'idle':
            info.status = 'Away';
            break;
        case 'dnd':
            info.status = 'Do Not Disturb';
            break;
        default:
            info.status = 'N/A'
            break;
    }
    
    // generate creation date
    info.createDate = user.createdAt.toLocaleString('en-US', config.bot.dateFormat);

    // generate join date
    if (!info.member) {
        info.joinDate = 'Not in Server';
    } else {
        info.joinDate = info.member.joinedAt.toLocaleString('en-US', config.bot.dateFormat);
    }

    if (user.presence.game) {
        info.game = user.presence.game.name;
    } else {
        info.game = 'None'
    }

    // generate role and permissions info
    if (info.member) {
        if (!member.hoistRole) {
            info.role = '@everyone';
        } else {
            info.role = `<@&${member.hoistRole.id}>`;
        }
        let perms = [];
        if (info.member.permissions.toArray().includes('ADMINSTRATOR')) {
            perms = ['Administrator'];
        } else {
            info.member.permissions.toArray().forEach(el => {
                let name = [];
                el.toLowerCase().split(/_+/g).forEach(sl => {
                    let ignore = [
                        'vad',
                        'tts'
                    ];
                    if (ignore.includes(sl)) {
                        return name.push(sl.toUpperCase());
                    }
                    name.push(sl.replace(/^./, sl.charAt(0).toUpperCase()));
                });
                name = name.join(' ');
                perms.push(name);
            })
        }
        let permString = perms.slice(0, 11).join(', ').trim();
        if (perms.length > 12) {
            permString+=`, ${perms.length - 12} more...`
        }
        info.perms = permString;
    } else {
        info.perms = 'N/A'
        info.role = 'N/A'
    }


    message.channel.send({
        embed: {
            "author": {
                "name": user.tag,
                "icon_url": user.avatarURL
            },
            "fields": [
                {
                    "name": "Account Creation Date",
                    "value": info.createDate,
                    "inline": true
                },
                {
                    "name": "Server Join Date",
                    "value": info.joinDate,
                    "inline": true
                },
                {
                    "name": "Status",
                    "value": info.status,
                    "inline": true
                },
                {
                    "name": "Playing",
                    "value": info.game,
                    "inline": true
                },
                {
                    "name": "Avatar",
                    "value": `[Click Here](${user.avatarURL})`,
                    "inline": true
                },
                {
                    "name": "Role",
                    "value": info.role,
                    "inline": true
                },
                {
                    "name": "Permissions",
                    "value": info.perms,
                    "inline": true
                }
            ]
        }
    });
}