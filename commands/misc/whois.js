exports.info = {
    "alias": "whois",
    "desc": "Performs a whois lookup on any given hostname.",
    "usage": "whois <hostname>",
    "category": "Miscellaneous",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , config = data.config
        , longArg = data.longArg;

    let whois = require('whois-parsed');

    whois.lookup(longArg).then(data => {
        message.channel.send({
            'embed': {
                'title': data.domainName,
                'description': `Registered by ${data.registrar}`,
                'fields': [
                    {
                        'name': 'Updated',
                        'value': new Date(data.updatedDate).toLocaleString('en-US', config.dateFormat),
                        'inline': true
                    },
                    {
                        'name': 'Created',
                        'value': new Date(data.creationDate).toLocaleString('en-US', config.dateFormat),
                        'inline': true
                    },
                    {
                        'name': 'Expires On',
                        'value': new Date(data.expirationDate).toLocaleString('en-US', config.dateFormat),
                        'inline': true
                    },
                    {
                        'name': 'Status',
                        // TODO: stop putting functions inside template literals; it's bad
                        'value': `${(() => {if (data.isAvailable) {return 'Available'} else {return 'Unavailable'}})()}`
                    }
                ]
            }
        });
    });
    
}