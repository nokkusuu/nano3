exports.info = {
    "alias": "eval",
    "desc": "Evaluates/executes code. (Developers Only)",
    "usage": "eval <code block>",
    "category": "Development",
    "hideInHelp": true,
    "reqPerms": ["DEVELOPER"]
}

exports.exec = (data) => {
    let client = data.client
        , message = data.message
        , commands = data.commands
        , longArg = data.longArg;
    
    // stuff that makes it easier for the user to do simple things
    let send = (msg) => {message.channel.send(msg)};
    let mention = (id) => {
        if (id.match(/[^\\d]/g)) {
            return;
        }
        return `<@${id}>`;
    }
    // function that grabs code from a code block
    function getCode(string) {
        let firstOccur = string.indexOf('```');
        if (firstOccur == -1) return message.channel.send(`<@${message.author.id}>, you cannot evaluate code without a valid code block.`);
        let secondOccur = string.indexOf('```', firstOccur+1)+3;
        if (secondOccur == -1) return message.channel.send(`<@${message.author.id}>, you cannot evaluate code without a valid code block.`);

        let code = string.substring(firstOccur, secondOccur);
        code = code.replace(/(```)(js|javascript)?/g, '');
        return code.trim();
    }
    
    eval(getCode(longArg));
}