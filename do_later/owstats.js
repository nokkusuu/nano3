/* TODO: fix this command

might help:
https://github.com/andrewbrooke/Overwatch-Discord-Stats-Bot/blob/master/overwatch.js
*/

exports.info = {
    "alias": "owstats",
    "desc": "Returns Overwatch stats for a specific username.",
    "usage": "ostats <battletag>",
    "category": "Gaming",
    "hideInHelp": false,
    "reqPerms": []
}

exports.exec = async (data) => {
    const client = data.client
        , message = data.message
        , commands = data.commands
        , longArg = data.longArg;

    const overwatch = require('overwatch-api')
        , platform = 'pc'
        , region = 'global';

    const fs = require('fs');

    // find battletag
    let tag = longArg.match(/\S+#[0-9]+/g);
    if (!tag) return message.channel.send(`<@${message.author.id}>, you must specify a valid battletag.`);
    rtag = tag[0].replace('#','-');

    // because the developer of the module was dumb enough not to make it async in the first place
    async function getData(platform, region, tag) {
        return new Promise(async (resolve, reject) => {
            async function getProfile(platform, region, tag) {
                return new Promise((resolve, reject) => {
                    overwatch.getProfile(platform, region, tag, (err, json) => {
                        if (err) reject(err);
                        resolve(json);
                    })
                })
            }
            async function getStats(platform, region, tag) {
                return new Promise((resolve, reject) => {
                    overwatch.getStats(platform, region, tag, (err, json) => {
                        if (err) reject(err);
                        resolve(json);
                    })
                })
            }
            let profile = await getProfile(platform, region, tag);
            let stats = await getStats(platform, region, tag);
            resolve({
                'profile': profile,
                'stats': stats
            });
        })
    }

    let msg = await message.channel.send(`Searching for ${tag[0]}...`);
    let dat = await getData(platform, region, rtag);
    fs.writeFileSync('owdump.json', JSON.stringify(dat));

    msg.delete();
    message.channel.send({
        'embed': {
            'author': {
                'name': dat.stats.username
            },
            'fields': [
                {
                    'name': 'Level',
                    'value': dat.profile.level
                },
                {
                    'name': 'Endorsements',
                    'value': `<:shot_caller:527574826353819669> ${dat.stats.endorsement.shotcaller.rate}% <:good_teammate:527574826404020225> ${dat.stats.endorsement.teammate.rate}% <:sportsmanship:527574826571792414> ${dat.stats.endorsement.sportsmanship.rate}%`
                }
            ]
        }
    })
}
