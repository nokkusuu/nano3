# nano-3.0
a fully-modular discord bot with cool recursive functions and stuff
currently wip, collectible system and some db-related commands are currently not implemented

## important
i promise, i'm not a script kiddie; this isn't some template i found online. originally, it was going to be entered into a discord bot contest, but over time i started to really love what i came up with, so i used it as a starting point to rewrite my own bot.\\

## what to look for (proof i know what i'm doing)
1. in `./util/cmdLoader.js`, there is a recursive function to read the contents of the `./commands` folder. iirc this was written entirely by hand. i'm sure it can be improved on, but i'm very proud of it, especially as it was my first truly recursive function *ever*.
2. wrote a small lib in `./util/extImg.js` to extract an image from a message, including those in urls. might have some conditions where it doesn't work, but i've not seen that happen yet, even with my friends spamming the bot's image commands.
3. used the least amount of packages as i could. the `gm` package isn't really necessary, as i could just start a child process, but that's a goal i have for the future.
4. used a `.env` file for secrets rather than json. this is more of a given, considering json isn't normally omitted using a gitignore. 

## things that could be improved
1. the ban command is WIP. months ago i attempted to make the ban command accept multiple users *and* a combination of both user ids and mentions. i haven't touched it in a while, but i remember getting stuck due to some quirk in js's scope system.
2. i use a bit of "top-level" (as in, top level of each command file) `await` here and there only for the purpose of avoiding a stupid amount of callbacks making the code difficult to read. sadly, this can sometimes cause errors to be handled incorrectly.
3. the help command goes through the entire commands array *every single time it runs*. later versions will generate it on startup.
4. inconsistency.